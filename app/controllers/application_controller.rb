class ApplicationController < ActionController::Base
	helper_method :current_user
	def current_user
		@user ||= User.find_by(auth_token: session[:access_token])
	end
end
