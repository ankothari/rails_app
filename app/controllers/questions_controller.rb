require "uri"
require "net/http"
require 'json'
require 'will_paginate/array'
class QuestionsController < ApplicationController
  def index
    if not session[:access_token]
      uri = URI('http://localhost:5000/top')
      x = Net::HTTP.get(uri) # => String
      @top = JSON.parse(x)
      # begin
      #   if params.has_key?(:page)
      #     params = {'word': "data-structures", 'page': params[:page]}
      #   else
      #     params = {'word': "data-structures"}
      #   end
      # rescue
      #   pp 'params not found'
      # end
      # params = {'word': "data-structures"}
      # x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), params)
      # body = x.body
      # @final_data = JSON.parse(body)
      # @questions = @final_data["all_questions"]
      # @questions = @questions.paginate(page: params[:page], per_page: 10)
      # # @questions = Kaminari.paginate_array(@questions).page(params[:page]).per(10)
      # @pending_questions = @final_data["pending_questions"]
      # @paginatable_array = LinkedTag.order(score: :desc).page(params[:page]).per(20)
      @tags = Tag.all.limit(5).uniq
      @paginatable_array = Kaminari.paginate_array(LinkedTag.order("RANDOM()")).page(params[:page]).per(30)
    else
      uri = URI('http://localhost:5000/top')
      x = Net::HTTP.get(uri) # => String
      @top = JSON.parse(x)
      # begin
      #   if params.has_key?(:page)
      #     params = {'word': "data-structures", 'page': params[:page]}
      #   else
      #     params = {'word': "data-structures"}
      #   end
      # rescue
      #   pp 'params not found'
      # end
      # params = {'word': "data-structures"}
      # x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), params)
      # body = x.body
      # @final_data = JSON.parse(body)
      # @questions = @final_data["all_questions"]
      # @questions = @questions.paginate(page: params[:page], per_page: 10)
      # # @questions = Kaminari.paginate_array(@questions).page(params[:page]).per(10)
      # @pending_questions = @final_data["pending_questions"]
      # @paginatable_array = LinkedTag.order(score: :desc).page(params[:page]).per(20)
      @tags = current_user.following_by_type('Tag').order(count: :desc).uniq
      @paginatable_array = Kaminari.paginate_array(LinkedTag.order("RANDOM()")).page(params[:page]).per(30)
    end
  end

  def answers
    get_params = {'access_token': session[:access_token],'site' => 'stackoverflow', 'key' => '6)pzbLQnfOokuC2n)6Xviw(('}
    uri = URI('https://api.stackexchange.com/2.2/me/answers')
    uri.query = URI.encode_www_form(get_params)

    res = Net::HTTP.get_response(uri)

    @final_data = JSON.parse(res.body)
    current_user.update_attributes(answers_response: JSON.parse(res.body), answers_response_updated_at: DateTime.now)
    @final_data["items"].each do |item|
      uri = URI('http://localhost:5000/question?id=' +item["question_id"].to_s)
      x = Net::HTTP.get(uri) # => String
      answers = JSON.parse(x)
      item['highest_score'] = answers["results"][0]["ascore"].to_s
      item['highest_score_id'] = answers["results"][0]["answer_id"].to_s
      item['question'] = answers["results"][0]["question"].to_s
      item['question_id'] = answers["results"][0]["question_id"].to_s
      uri = URI("https://api.stackexchange.com/2.2/questions/#{item['question_id']}/linked?order=desc&sort=activity&site=stackoverflow")
      res = Net::HTTP.get_response(uri)
      item['similar_questions'] = JSON.parse(res.body)
      uri = URI("https://api.stackexchange.com/2.2/questions/#{item['question_id']}/related?order=desc&sort=activity&site=stackoverflow")
      res = Net::HTTP.get_response(uri)
      item['related_questions'] = JSON.parse(res.body)
    end

    uri = URI('https://api.stackexchange.com/2.2/me')
    uri.query = URI.encode_www_form(get_params)
    res = Net::HTTP.get_response(uri)
    @me = JSON.parse(res.body)
    current_user.update_attributes(name: @me['items'][0]['display_name'], reputation: @me['items'][0]['reputation'])
  end

  def signed_in
    code = params['code']

    params = {'client_id' => 12430, 'code' => code, 'client_secret' => '9*Kyrtrtb*iwc6v4soDAuw((', 'redirect_uri' => 'http://localhost:3000/questions/signed_in'}
    x = Net::HTTP.post_form(URI.parse('https://stackoverflow.com/oauth/access_token'), params)
    access_token = x.body.split("=")[1]
    if User.where(auth_token: access_token).present?
    else
      User.create(auth_token: access_token)
    end
    session[:access_token] = access_token
    
    # uri = URI('https://api.stackexchange.com/2.2/me')
    # uri.query = URI.encode_www_form(get_params)
    #
    # res = Net::HTTP.get_response(uri)
    # if res.is_a?(Net::HTTPSuccess)
    #   @final_data = JSON.parse(res.body)
    #   @reputation = @final_data["items"][0]["reputation"]
    # else
    #   @final_data = res.body
    # end

    
    # if has logged in, show questions only relevant to him. Show him
    # how his reputation has increased. And also find some great questions for him to learn from
    # and also learn.

    # also show quetions similar to the ones he has read, sorted by the number of minutes he spent on them.

    # send him questions daily to follow or to answer.

    # help the user build his profile.

    # after this is done, do the same for github.

    # also find if u can use linkedin to see what the user knows also based on the resume he gives.
    redirect_to action: 'index'
  end
  def top
    params = {'word': "data-structures"}
    x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), params)
    body = x.body
    @final_data = JSON.parse(body)
    @questions = @final_data["all_questions"]
    @questions = @questions.paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.js
    end
  end

  def search_by_id
    
  end

  def search_id
    params = {'id': request.params['question_id']}
    x = Net::HTTP.post_form(URI.parse('http://localhost:5000/search_by_id'), params)
    body = x.body
    @final_data = JSON.parse(body)
    @questions = @final_data["questions"]
    @pending_questions = @final_data["pending_questions"]
  end

  def search
    params = {'word': request.params['q']}
    x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), params)
    body = x.body
    @final_data = JSON.parse(body)
    @questions = @final_data["data"]
    @pending_questions = @final_data["pending_questions"]
    tag_id = []
    @final_data['related_tags'].each do |tag|
      pp tag
      tag_id.append(tag.values()[0]['id'])
    end
    pp tag_id
    @linked_tags = LinkedTag.where(related_tag_id: tag_id)
    @tags = Tag.where(id: @linked_tags.pluck(:tag_id))
    # Show similar tags as well after search. 
    @paginatable_array = Kaminari.paginate_array(@linked_tags).page(params[:page]).per(30)
  end

  def similar
    uri = URI("https://api.stackexchange.com/2.2/questions/#{params['question_id']}/linked?order=desc&sort=activity&site=stackoverflow")
    res = Net::HTTP.get_response(uri)
    @similar_questions = JSON.parse(res.body)
    current_user.follow(LinkedQuestion.find_by(related_post_id: params['question_id']))
  end

  def explore
    uri = URI("https://api.stackexchange.com/2.2/questions/#{params['question_id']}/related?order=desc&sort=activity&site=stackoverflow")
    res = Net::HTTP.get_response(uri)
    @related_questions = JSON.parse(res.body)
  end
end