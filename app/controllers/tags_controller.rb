require "uri"
require "net/http"
require 'json'
require 'will_paginate/array'
class TagsController < ApplicationController
	def index
		if params[:name]
		  uri = URI("http://localhost:5000/search_tag?search_tag="+params[:name])
      x = Net::HTTP.get(uri) # => String
      body = JSON.parse(x)
      @user_following_tags = current_user.following_by_type('Tag').where(id: Tag.where(name: body.keys()).pluck(:id)).order(count: :desc)
		  @tags = Tag.where(name: body.keys()).order(count: :desc).where(id: Tag.all.pluck(:id)-@user_following_tags.pluck(:id)).order(count: :desc).page params[:page]
	  else
			@tags = Tag.all.order(count: :desc).page params[:page]
		end
	end

	def follow
		@tag = Tag.find(params[:id])
		User.find_by(auth_token: session[:access_token]).follow(@tag) 
	end

	def unfollow
		@tag = Tag.find(params[:id])
		User.find_by(auth_token: session[:access_token]).stop_following(@tag) 
	end


	def show 
		custom_params = {'word': Tag.find(params["id"]).name}
		x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), custom_params)

    body = x.body
    @final_data = JSON.parse(body)
    @name = Tag.find(params["id"]).name
  end
end
