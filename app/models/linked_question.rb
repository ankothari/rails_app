class LinkedQuestion < ApplicationRecord
	acts_as_followable
	
	has_many :learns
	has_many :users, through: :learns
end
