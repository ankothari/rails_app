class User < ApplicationRecord
	acts_as_follower
	has_many :learns
	has_many :linked_questions, :through => :learns
end
