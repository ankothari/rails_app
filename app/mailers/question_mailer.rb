require "uri"
require "net/http"
require 'json'
class QuestionMailer < ApplicationMailer
  default from: 'daily_digest@stackoverflow.com'

  def send_daily_questions()
    @user = "Ankur Kothari"
    @url  = 'http://example.com/login'

    params = {'word': "depth-first-search"}
    x = Net::HTTP.post_form(URI.parse('http://localhost:5000/result'), params)
    body = x.body
    @final_data = JSON.parse(body)

    mail(to: @user, final_data: @final_data, subject: "Daily Questions to Ponder")
  end
end
