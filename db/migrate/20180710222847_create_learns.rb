class CreateLearns < ActiveRecord::Migration[5.2]
  def change
    create_table :learns do |t|
      t.integer :user_id
      t.integer :linked_question_id
      t.timestamps
    end
  end
end
