class AddAnswersResponseToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :answers_response, :text
  end
end
