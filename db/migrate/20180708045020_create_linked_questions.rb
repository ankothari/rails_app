class CreateLinkedQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :linked_questions do |t|
      t.integer :related_post_id
      t.string :title

      t.timestamps
    end
  end
end
