class AddAnswersResponseUpdatedAtToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :answers_response_updated_at, :datetime
  end
end
