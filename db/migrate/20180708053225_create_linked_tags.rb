class CreateLinkedTags < ActiveRecord::Migration[5.2]
  def change
    create_table :linked_tags do |t|
      t.integer :count
      t.integer :score
      t.integer :related_tag_id

      t.timestamps
    end
  end
end
