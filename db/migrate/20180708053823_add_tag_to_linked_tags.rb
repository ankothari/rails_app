class AddTagToLinkedTags < ActiveRecord::Migration[5.2]
  def change
    add_column :linked_tags, :tag, :string
  end
end
