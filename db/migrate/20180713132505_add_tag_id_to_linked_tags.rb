class AddTagIdToLinkedTags < ActiveRecord::Migration[5.2]
  def change
    add_column :linked_tags, :tag_id, :integer
  end
end
