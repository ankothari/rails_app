class AddIndexToLinkedQuestionsRelatedPostId < ActiveRecord::Migration[5.2]
  def change
  	add_index :linked_questions, :related_post_id
  end
end
