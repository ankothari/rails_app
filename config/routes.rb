Rails.application.routes.draw do
  root to: 'questions#index'
  resources :questions, only: :index
  get 'questions/search' => 'questions#search'
  get 'questions/top' => 'questions#top'
  get 'questions/signed_in' => 'questions#signed_in'
  get 'questions/answers' => 'questions#answers'
  get 'questions' => 'questions#index'
  resources :tags
  resources :tags do
    get :autocomplete_tag_name, :on => :collection
  end

  get 'questions/similar'
  get 'questions/search_by_id'
  post 'questions/search_id'
  get 'questions/explore'
  post 'tags/follow'
  post 'tags/unfollow'
  get 'profile' => 'profile#show'
end
